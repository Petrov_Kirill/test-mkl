#include <stdio.h>
#include <stdlib.h>
#include "mkl_lapacke.h"

#define N 5
#define NRHS 3
#define LDA N
#define LDB N


int main() {

  MKL_INT n = N, nrhs = NRHS, lda = LDA, ldb = LDB, info;
  /* Data */
  MKL_INT ipiv[N];
  double a[LDA * N] = {
      -5.86, 3.99, -5.93, -2.82, 7.69,
      0.00, 4.46, 2.58, 4.42, 4.61,
      0.00, 0.00, -8.52, 8.57, 7.69,
      0.00, 0.00, 0.00, 3.72, 8.07,
      0.00, 0.00, 0.00, 0.00, 9.83
  };
  double b[LDB * NRHS] = {
      1.32, 2.22, 0.12, -6.41, 6.33,
      -6.33, 1.69, -1.56, -9.49, -3.67,
      -8.77, -8.33, 9.54, 9.56, 7.48
  };

  printf("LAPACKE_dsysv (column-major, high-level) Results\n");

  info = LAPACKE_dsysv(LAPACK_COL_MAJOR, 'L', n, nrhs, a, lda, ipiv, b, ldb);

  if (info > 0) {
    printf("The element of the diagonal factor ");
    printf("D(%i,%i) is zero, so that D is singular;\n", info, info);
    printf("the solution could not be computed.\n");
    exit(1);
  }
  exit(0);
}