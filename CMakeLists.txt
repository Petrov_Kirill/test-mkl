cmake_minimum_required(VERSION 3.5)

find_program(ICC_PATH  NAMES icc)
find_program(ICPC_PATH NAMES icpc)
message(${ICC_PATH})
message(${ICPC_PATH})

if(${ICC_PATH} MATCHES ICC_PATH-NOTFOUND OR
        ${ICPC_PATH} MATCHES ICPC_PATH-NOTFOUND)
  message(STATUS "Cannot find Intel compiler. You may need to run `. /opt/intel/bin/compilervars.sh intel64'")
else()

  set(CMAKE_C_COMPILER     ${ICC_PATH})
  set(CMAKE_CXX_COMPILER   ${ICPC_PATH})

  set(CMAKE_MODULE_PATH ${CMAKE_HOME_DIRECTORY}/cmake/modules)

  find_package(MKL REQUIRED)
  include_directories(${MKL_INCLUDE_DIRS})
  link_directories(${MKL_LIBRARIES})

endif()

project(TEST_INTEL_PROJECT)

set(SOURCE_FILES main.cpp)
add_executable(TEST_INTEL_PROJECT ${SOURCE_FILES})

target_link_libraries(TEST_INTEL_PROJECT
        mkl_intel_lp64
        mkl_sequential
        mkl_core
        )
